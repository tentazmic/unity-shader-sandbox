﻿Shader "Playground/SpecularLight"
{
    Properties
    {
    	_Color ("Surface Color", Color) = (1, 1, 1, 1)
    	_AmbientColor ("Ambient Color", Color) = (.1, .2, .2, 1)
    	_Gloss ("Gloss", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            
            float4 _Color;
            float4 _AmbientColor;
            float _Gloss;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float3 worldPos : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.normal = v.normal;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }
            
            float posterize(float steps, float value)
            {
            	return floor(value * steps) / steps;
            }

            fixed4 frag (v2f i) : SV_Target
            {
            	float3 normal = normalize(i.normal); // Interpolated
            
                // Lighting
            	float3 lightColor = _LightColor0.rgb;
            	float3 lightDir = _WorldSpaceLightPos0.xyz;
            	
                // Direct Diffuse Lighting
            	float lightFalloff = max(0, dot(lightDir, normal));
            	lightFalloff = posterize(16, lightFalloff);
            	//lightFalloff = step(0.6, lightFalloff);
            	float3 directDiffuseLight = lightFalloff * lightColor;
            	
            	// Direct Specular Lighting
            	float3 cameraPos = _WorldSpaceCameraPos;
            	float3 viewDir = normalize(cameraPos - i.worldPos);
            	
            	float3 viewReflect = reflect(-viewDir, normal);
            	
            	float specularFalloff = max(0, dot(viewReflect, lightDir));
            	specularFalloff = posterize(2, specularFalloff);
            	// specularFalloff = step(0.8, specularFalloff);
            	
            	// Modify by Gloss
            	specularFalloff = pow(specularFalloff, _Gloss);
            	
            	float3 directSpecular = specularFalloff * lightColor;
            	
            	// return specularFalloff;
            	// Phong
            	
            	
            	// Composite
            	float3 diffuseLight = directDiffuseLight + _AmbientColor;
            	float3 finalColor = diffuseLight * _Color.rgb + directSpecular;
            	
                return float4(finalColor, 0);
            }
            ENDCG
        }
    }
}
