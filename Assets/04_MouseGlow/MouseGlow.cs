﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class MouseGlow : MonoBehaviour
{
	Plane _plane;
	Camera _camera;
	
	void Start()
	{
		_plane = new Plane(Vector3.up, Vector3.zero);
		_camera = GetComponent<Camera>();
	}

	void Update()
	{
		Vector2 mousePos = Input.mousePosition;
		var ray = _camera.ScreenPointToRay(mousePos);

		if (_plane.Raycast(ray, out float enterDistance))
		{
			Vector3 worldMousePos = ray.GetPoint(enterDistance);
			Shader.SetGlobalVector("_MousePos", worldMousePos);
		}
	}
}
