﻿Shader "Playground/MouseGlow"
{
    Properties
    {
        _Color ("Surface Color", Color) = (.5, .3, .2, 1)
        _AmbientColor ("Ambient Color", Color) = (.1, .1, .2, 1)
        _Gloss ("Gloss", Range(0.1, 500)) = 1
        
        _DiffuseSteps ("Diffuse Steps", Float) = 8
        _SpecularSteps ("Specular Steps", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
                float3 worldPos : TEXCOORD1;
            };
            
            uniform float3 _MousePos;

			float4 _Color;
			float4 _AmbientColor;
			float _Gloss;
			
			float _DiffuseSteps;
			float _SpecularSteps;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.normal = v.normal;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }
            
            float posterize(float steps, float value)
            {
            	return floor(value * steps) / steps;
            }

            fixed4 frag (v2f i) : SV_Target
            {
            	float3 normal = normalize(i.normal);
            
            	// Rendering The Color
            	float3 lightColor = _LightColor0.rgb;
            	float3 lightDir = _WorldSpaceLightPos0.xyz;
            	
            	// Diffuse
            	float lightFalloff = max(0, dot(lightDir, normal));
            	lightFalloff = posterize(floor(_DiffuseSteps), lightFalloff);
            	float3 directDiffuseLight = lightFalloff * lightColor;
            	
            	// Direct Spec
            	float3 viewDir = 
            		normalize(_WorldSpaceCameraPos - i.worldPos);
            	float3 viewReflect = reflect(-viewDir, normal);
            	
            	float specularFalloff = max(0, dot(viewReflect, lightDir));
            	specularFalloff = posterize(floor(_SpecularSteps), specularFalloff);
            	
            	// Modify by Gloss
            	specularFalloff = pow(specularFalloff, _Gloss);
            	float3 directSpec = specularFalloff * lightColor;
            	
            	float3 diffuseLight = directDiffuseLight + _AmbientColor;
            	float3 finalColor = diffuseLight * _Color.rgb + directSpec;
            
                float distToCursor = distance(_MousePos, i.worldPos);
                float glow = saturate(1 - distToCursor);
                
                finalColor += glow.xxx;
                return float4(finalColor, 0);
            }
            ENDCG
        }
    }
}
