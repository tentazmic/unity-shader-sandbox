﻿Shader "Playground/SimpleLighting"
{
    Properties
    {
    	_Color ("Surface Color", Color) = (1, 1, 1, 1)
    	_AmbientColor ("Ambient Color", Color) = (.1, .2, .2, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            
            float4 _Color;
            float4 _AmbientColor;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.normal = v.normal;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
            	// Lighting
            	float3 lightColor = _LightColor0.rgb;
            	float3 lightDir = _WorldSpaceLightPos0.xyz;
            	
            	float lightFalloff = max(0, dot(lightDir, i.normal));
            	float3 directDiffuseLight = lightFalloff * lightColor;
            	
            	float3 diffuseLight = directDiffuseLight + _AmbientColor;
            	float3 finalColor = diffuseLight * _Color.rgb;
            	
                return float4(finalColor, 0);
            }
            ENDCG
        }
    }
}
