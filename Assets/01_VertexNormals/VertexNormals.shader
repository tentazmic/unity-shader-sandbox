﻿Shader "Playground/VertexNormals"
{
    Properties
    {
//        _MainTex ("Texture", 2D) = "white" {}
		_useNormals ("Use Normals", Range(0, 1)) = 1
		_normalMultiplier ("Normal Multiplier", Range(0, 1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            // #pragma multi_compile_fog

            #include "UnityCG.cginc"
            
            float1 _useNormals;
            float1 _normalMultiplier;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = v.normal;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
            	float3 normal = i.normal;
            	normal = normal * .5 + (_useNormals * _normalMultiplier);
            	
                return float4(normal, 1);
            }
            ENDCG
        }
    }
}
