﻿Shader "BookOfShaders/Time"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };
            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }
            
            float plot(float2 st, float pct)
            {
            	return smoothstep(pct - 0.02, pct, st.y) -
            		smoothstep(pct, pct + 0.02, st.y);
            }

            float3 blend(float3 a, float3 b, float pct)
            {
	            return (1 - pct) * a + pct * b;
            }
            
            float3 colorA = float3(0.149,0.141,0.912);
            float3 colorB = float3(1.000,0.833,0.224);

            fixed4 frag (v2f i) : SV_Target
            {
            /*
            	float x = smoothstep(0.2, 0.5, i.uv.x) - smoothstep(0.5, 0.8, i.uv.x);
            	float3 color = x.xxx;
            	
            	float pct = plot(i.uv, x);
            	color = (1 - pct) * color + pct * float3(0, 1, 0); */
            	
            	float pct = abs(sin(_Time.y));
            	
            	float3 color = blend(colorA, colorB, pct);
            	
                return float4(color, 1);
            }
            ENDCG
        }
    }
}
